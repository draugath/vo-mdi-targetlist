local config_name = "MDI TargetList"
local default_target_color = "65 255 65 65 *"

declare("targetlist", {
	version = "3.1.0",
	config_name = config_name,
	hide_turrets = gkini.ReadInt(config_name, "Hide Turrets", 0) == 1,
	invert_faction_filter = gkini.ReadInt(config_name, "Invert Faction Filter", 0) == 1,
	filter = gkini.ReadInt(config_name, "Filter", 0), -- .filters will hold the IUP structure
	faction_filter = gkini.ReadInt(config_name, "Faction Filter", 0),
	fontsize = gkini.ReadInt(config_name, "FontSize", Font.H5),
	current_target_color = gkini.ReadString(config_name, "Highlight", default_target_color),
	list_count = gkini.ReadInt(config_name, "List Count", 10),
	name_width = gkini.ReadInt(config_name, "Name Width", 0),
	x = gkini.ReadInt(config_name, "x", -1),
	y = gkini.ReadInt(config_name, "y", -1),
	default_target_color = default_target_color,
	list_offset = 1,
	playerlist = {},
	shiplist = {},
	id = "targetlist", -- used in MDI
	refresh_timer = Timer(),
	refresh_delay = 1000,
	current_target_listindex,
	current_target_nodeid,
	current_target_objectid,
})
if (targetlist.x == -1) then targetlist.x = nil end
if (targetlist.y == -1) then targetlist.y = nil end
if (targetlist.fontsize == 0) then targetlist.fontsize = nil end
if (targetlist.name_width == 0) then targetlist.name_width = nil end

console_print("MDI-TargetList loaded (v"..targetlist.version..").")

local function IsDeclared(value) return pcall(loadstring("return "..value)) end
	
local function IsIUP(control) return control and pcall(iup.GetType, control) end

local function loadluafile(...)
	local f, err = loadfile((...))
	assert(f and type(f) == "function", err)
	return f(select(2, ...))
end

local capship_type_names = {
	["Capella"] = true,
	["Goliath"] = true,
	["Heavy Assault Cruiser"] = true,
    ["Leviathan"] = true,
	["TPG Teradon Frigate"] = true,
    ["Trident Light Frigate"] = true,
    ["TPG Constellation Heavy Transport"] = true
}

local function filter_players(factionid)
	if (targetlist.faction_filter == 0) then
		return true
	elseif (not targetlist.invert_faction_filter and factionid == targetlist.faction_filter) then
		return true
	elseif (targetlist.invert_faction_filter and factionid ~= targetlist.faction_filter) then
		return true
	end
	return false
end

local function scan_player(charid)
	if (not charid) then return end
	local distance = GetRadarDistance(charid)
	local insert_player = false
	if (distance) then
		local name = GetPlayerName(charid)
		local factionid = GetPlayerFaction(charid)
		if     (targetlist.hide_turrets and name:match("\*.*Turret")) then
		    --do nothing

		elseif (targetlist.filter == 0) then -- All
			insert_player = filter_players(factionid)

		elseif (targetlist.filter == 1 and name:match("^\*")) then -- NPCs
			insert_player = filter_players(factionid)

		elseif (targetlist.filter == 2 and name:match("^[^*]"))then -- PCs
			insert_player = filter_players(factionid)

		elseif (targetlist.filter == 3) then -- Capships
			if (filter_players(factionid)) then
				local ship_name = GetPrimaryShipNameOfPlayer(charid)
				if (capship_type_names[ship_name] or (ship_name == "Robot" and name:match("Queen"))) then
					insert_player = true
				end
			end
		end
		if (insert_player) then
			table.insert(targetlist.shiplist, {
				charid = charid,
				factionid = factionid,
				name = name,
				distance = math.floor(distance),
			})
		end
	end
	
	return scan_player(table.remove(targetlist.playerlist))
end

local function scan_sector()
	targetlist.refresh_timer:Kill()
	targetlist.playerlist = {}
	targetlist.shiplist = {}

	ForEachPlayer(function(charid)
		if (charid ~= GetCharacterID()) then table.insert(targetlist.playerlist, charid) end
	end)
	scan_player(table.remove(targetlist.playerlist)) -- initiates a recursive tail call to iterate over the entire player list
	table.sort(targetlist.shiplist, function(a, b) return a.distance < b.distance end)
	targetlist.display_container.update_display()

	targetlist.refresh_timer:SetTimeout(targetlist.refresh_delay, scan_sector)
end

loadluafile("display.lua", targetlist, scan_sector)
local change_target = loadluafile("commands.lua", targetlist)
loadluafile("events.lua", targetlist, IsDeclared, IsIUP, scan_sector)
loadluafile("touch.lua", targetlist, change_target)
loadluafile("help.lua", targetlist)
