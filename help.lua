local project_tbl = ...

project_tbl.help = {
	help = [=[
MDI-Targetlist Command Syntax
   /targetlist commands
   /targetlist font [<size> | reset]
   /targetlist help [<subject>]
   /targetlist highlight [<r> <g> <b> <a> | reset]
   /targetlist pos [[+|-]<x> [+|-]<y> | reset]
   /targetlist rows [<rows> | reset]
   /targetlist width [<width> | reset]]=],
	
	commands = [=[
MDI-Targetlist Commands
   cyclefilter            -- cycles the filter between All, NPC, PC, and Capships
   cyclefactionfilter     -- cycles the faction filter between All, Itani, Serco, and UIT
   invertfactionfilter    -- inverts the filter to show everyone but the selected faction filter
   settargetbinds         -- binds target1 - target10 to the numbers 1-0, cyclefilter to "-", and cyclefactionfilter to "="
   target1 ... target10   -- set your target to the ship at that position in the list
   targetnext             -- select the next target in the list
   targetprev             -- select the previous target in the list
   toggleturrets          -- show or hide turrets in the list
]=],
	
	font = [=[
MDI-Targetlist Font Command Syntax
   /targetlist font          -- displays the current font size
   /targetlist font <font>   -- sets the fontsize to the specifed size (e.g. /targetlist font 12)
   /targetlist font reset    -- resets the fontsize to the default [client dependent]
]=],
             
	highlight = [=[
MDI-Targetlist Highlight Command Syntax
   /targetlist highlight                   -- displays the current highlight color
   /targetlist highlight <r> <g> <b> <a>   -- sets the current target highlight to the specified color (e.g. /targetlist 65 255 65 65)
   /targetlist highlight reset             -- resets the highlight color to the default [65 255 65 65]
]=],
	
	pos = [=[
MDI-Targetlist Pos Command Syntax
   /targetlist pos                     -- displays the current position of the list window
   /targetlist pos [+|-]<x> [+|-]<y>   -- moves the list window to the position indicated.  If the optional + or - are used, it will nudge the window by the specified amount.
   /targetlist pos reset               -- resets the position to the default value
            Examples: 
           /targetlist pos 1000 200
           /targetlist pos +10 -5
           /targetlist pos 1000 +10
]=],
	
	rows = [=[
MDI-Targetlist Rows Command Syntax
   /targetlist rows          -- displays the current number of rows
   /targetlist rows <rows>   -- sets the number of rows shown in the list (e.g. /targetlist rows 15)
   /targetlist rows reset    -- resets the number of rows to the default [10]
]=],

	width = [=[
MDI-Targetlist Width Command Syntax
   /targetlist width           -- displays the current name width
   /targetlist width <width>   -- sets the name width top the indicated width (e.g. /targetlist width 100)
   /targetlist width reset     -- resets the name width to the default [font dependent]
]=],
}
