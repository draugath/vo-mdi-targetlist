local project_tbl = ...

function verify_number(x, l, u)
	if (x) then
		if (l) then l = x >= l else l = true end
		if (u) then u = x <= u else u = true end
		if (l and u) then return x end
	end
	return false
end

local handler = {
	["commands"] = function(data)
		print("\127FFFFFF"..project_tbl.help["commands"])
	end,
	
	["pos"] = function(data)
		if (project_tbl.hasmdi) then return end

		local x, y = data[1], data[2]
		local container = project_tbl.display_container
		if (x and y) then
			if (x:match("^[+-]")) then x = container.cx + x end
			if (y:match("^[+-]")) then y = container.cy + y end
			container.cx, container.cy = tonumber(x), tonumber(y)
			HUD.cboxlayer.REFRESH = "YES"
			project_tbl.x = x
			project_tbl.y = y
			gkini.WriteInt(project_tbl.config_name, "x", x)
			gkini.WriteInt(project_tbl.config_name, "y", y)
			project_tbl:DestroyRegions()
			project_tbl:CreateRegions()

		else
			if (data[1] == "reset") then
				project_tbl.x = nil
				project_tbl.y = nil
				gkini.WriteInt(project_tbl.config_name, "x", -1)
				gkini.WriteInt(project_tbl.config_name, "y", -1)
				project_tbl:RebuildDisplay()

			else
				local pos = container.cx.."x"..container.cy
				if (not project_tbl.x) then pos = "unset (actual "..container.cx.."x"..container.cy..")" end
				print("\127FFFFFFTargetlist position is "..pos)
			end
		end
	end,
	
	["font"] = function(data)
		local newfont = tonumber(data[1])
		if (newfont and newfont > 0) then
			project_tbl.fontsize = newfont
			gkini.WriteInt(project_tbl.config_name, "FontSize", project_tbl.fontsize)
			print("\127FFFFFFFont size has been changed to "..newfont..".")
			project_tbl:RebuildDisplay()
			
		else
			if (data[1] == "reset") then
				project_tbl.fontsize = nil
				gkini.WriteInt(project_tbl.config_name, "FontSize", 0)
				print("\127FFFFFFThe font size has been been reset to "..Font.Default..".")
				project_tbl:RebuildDisplay()
			else
				local fontsize = project_tbl.fontsize
				if (not fontsize) then fontsize = "unset (actual "..Font.Default..")" end
				print("\127FFFFFFThe current font size is "..fontsize..".")
			end
		end
	end,
	
	["help"] = function(data)
		local word = table.remove(data, 1)
		if (word) then
			word = string.lower(tostring(word))
			if (type(project_tbl.help[word]) == "string") then
				print("\127FFFFFF"..project_tbl.help[word])
			end

		else
			print("\127FFFFFF"..project_tbl.help["help"])
		end
	end,
              
	["highlight"] = function(data)
		local r, g, b, a = tonumber(data[1]), tonumber(data[2]), tonumber(data[3]), tonumber(data[4])
		if (verify_number(r, 0, 255) and verify_number(g, 0, 255) and verify_number(b, 0, 255)) then
			a = verify_number(a, 0, 255) or 65
			project_tbl.current_target_color = string.format("%d %d %d %d *", r, g, b, a)
			gkini.WriteString(project_tbl.config_name, "Highlight", project_tbl.current_target_color)
			print("\127FFFFFFThe highlight color has been changed to "..project_tbl.current_target_color:match("(.*) %*"))
		
		else
			if (data[1] == "reset") then
				project_tbl.current_target_color = project_tbl.default_target_color
				gkini.WriteString(project_tbl.config_name, "Highlight", project_tbl.current_target_color)
				print("\127FFFFFFThe highlight color has been reset to "..project_tbl.current_target_color:match("(.*) %*"))
			else
				print("\127FFFFFFThe current highlight color is "..project_tbl.current_target_color:match("(.*) %*"))
			end
		end
	end,
	
	["rows"] = function(data)
		local newrows = tonumber(data[1])
		if (newrows and newrows > 0) then
			project_tbl.list_count = newrows
			gkini.WriteInt(project_tbl.config_name, "List Count", project_tbl.list_count)
			print("\127FFFFFFThe number of rows has been changed to "..newrows..".")
			project_tbl:RebuildDisplay()
		else
			if (data[1] == "reset") then
				project_tbl.list_count = 10
				gkini.WriteInt(project_tbl.config_name, "List Count", project_tbl.list_count)
				print("\127FFFFFFThe number of rows has been been reset to 10.")
				project_tbl:RebuildDisplay()
			else
				print("\127FFFFFFThe current number of rows is "..project_tbl.list_count..".")
			end
		end
	end,
	
	["width"] = function(data)
		local newwidth = tonumber(data[1])
		if (newwidth and newwidth > 0) then
			project_tbl.name_width = newwidth
			gkini.WriteInt(project_tbl.config_name, "Name Width", project_tbl.name_width)
			print("\127FFFFFFName width has been changed to "..newwidth..".")
			project_tbl:RebuildDisplay()

		else
			if (data[1] == "reset") then
				project_tbl.name_width = nil
				gkini.WriteInt(project_tbl.config_name, "Name Width", 0)
				print("\127FFFFFFName width has been reset.")
				project_tbl:RebuildDisplay()
			else
				local width = project_tbl.name_width
				if (not width) then width = "unset (actual "..project_tbl.display[1][2][3][2].w..")" end
				print("\127FFFFFFThe current name width is "..width..".")
			end
		end
	end
}

local function cmd_processor(_, data)
	if (not data) then 
		print("\127FFFFFF"..project_tbl.help["help"])
		return
	end
	local cmd = string.lower(table.remove(data, 1))
	if (type(handler[cmd]) == "function") then handler[cmd](data) end
end

local function cycle_filter(_, data)
	if (data) then data = tonumber(data[1]) % 4 end
	project_tbl.filters:SetHighlight(project_tbl.filter + 1, false)
	project_tbl.filter = data or ((project_tbl.filter + 1) % 4)
	project_tbl.filters:SetHighlight(project_tbl.filter + 1, true)
	gkini.WriteInt(project_tbl.config_name, "Filter", project_tbl.filter)
end

local function cycle_faction_filter()
	project_tbl.faction_filter = (project_tbl.faction_filter + 1) % 4
	project_tbl.filters:SetHighlight(project_tbl.filter + 1, true)
	
end

local function change_target(i)
	if (project_tbl.shiplist[i]) then
		radar.SetRadarSelection(project_tbl.shiplist[i].nodeid, project_tbl.shiplist[i].objectid)
	end
end

local function target_next()
	local i = project_tbl.current_target_listindex or 0
	i = (i + 1) % project_tbl.list_count
	if (i == 0) then i = project_tbl.list_count end
	if (i > #project_tbl.shiplist) then i = 1 end
	change_target(i)
end

local function target_prev()
	local i = project_tbl.current_target_listindex or 0
	i = (i - 1) % project_tbl.list_count
	if (i == 0) then i = project_tbl.list_count end
	if (i > #project_tbl.shiplist) then i = #project_tbl.shiplist end
	change_target(i)
end

local verb = {[true] = "HIDING", [false] = "SHOWING"} -- reversed logic because the variable is hide_turrets
local verb_color = {[true] = "\127FF0000", [false] = "\12700FF00"}
local function toggleturrets()
	project_tbl.hide_turrets = not project_tbl.hide_turrets
	gkini.WriteInt(project_tbl.config_name, "Hide Turrets", project_tbl.hide_turrets and 1 or 0)
	print(string.format("%sTargetList is now %s%s%s turrets.", "\127FFFFFF", verb_color[project_tbl.hide_turrets], verb[project_tbl.hide_turrets], "\127FFFFFF"))
end

local verb = {[true] = "INVERTED", [false] = "NORMAL"}
local function invert_faction_filter()
	project_tbl.invert_faction_filter = not project_tbl.invert_faction_filter
	gkini.WriteInt(project_tbl.config_name, "Invert Faction Filter", project_tbl.invert_faction_filter and 1 or 0)
	print(string.format("%sTargetList faction filter is now %s%s%s.", "\127FFFFFF", verb_color[project_tbl.invert_faction_filter], verb[project_tbl.invert_faction_filter], "\127FFFFFF"))
	iup.GetBrother(project_tbl.filters).bgcolor = "255 0 0 "..(project_tbl.invert_faction_filter and 35 or 0)
end


local function set_key_binds()
	for i = 1, project_tbl.list_count < 10 and project_tbl.list_count or 10 do 
		gkinterface.GKProcessCommand("bind "..(i % 10).." target"..i)
	end
	gkinterface.GKProcessCommand("bind - cyclefilter")
	gkinterface.GKProcessCommand("bind = cyclefactionfilter")
end

for i = 1, project_tbl.list_count < 10 and project_tbl.list_count or 10 do 
	RegisterUserCommand("target"..i, change_target, i)
end
RegisterUserCommand("cyclefilter", cycle_filter)
RegisterUserCommand("cyclefactionfilter", cycle_faction_filter)
RegisterUserCommand("invertfactionfilter", invert_faction_filter)
RegisterUserCommand("targetnext", target_next)
RegisterUserCommand("targetprev", target_prev)
RegisterUserCommand("toggleturrets", toggleturrets)
RegisterUserCommand("settargetbinds", set_key_binds)
RegisterUserCommand("targetlist", cmd_processor)

return change_target
