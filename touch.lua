local project_tbl, change_target = ...

local timer = Timer()

function project_tbl:CreateRegions()
	if (not gkinterface.IsTouchModeEnabled()) then return end

	local row = self.display[1 + project_tbl.list_offset][1]
	local lr_x = tonumber(row.x)
	local lr_y = tonumber(row.y)
	local lr_w = tonumber(row.w)
	local lr_h = tonumber(row.h)
	local lr_h = lr_h * project_tbl.list_count
	--                                              1    2    3    4    5    6    7    8    9    10    11    12           13           14
	self.listregion = gkinterface.CreateTouchRegion(nil, nil, nil, nil, nil, nil, nil, nil, nil, lr_x, lr_y, lr_x + lr_w, lr_y + lr_h, -1)

	local column = self.filters[1]
	local fr_x = tonumber(column.x)
	local fr_y = tonumber(column.y)
	local fr_w = tonumber(column.w)
	fr_w = fr_w * 4
	local fr_h = tonumber(column.h)
	--                                                1    2    3    4    5    6    7    8    9    10    11    12           13           14
	self.filterregion = gkinterface.CreateTouchRegion(nil, nil, nil, nil, nil, nil, nil, nil, nil, fr_x, fr_y, fr_x + fr_w, fr_y + fr_h, -1)
end

function project_tbl:DestroyRegions()
	if (not gkinterface.IsTouchModeEnabled()) then return end

	gkinterface.DestroyTouchRegion(self.listregion)
	gkinterface.DestroyTouchRegion(self.filterregion)
end

-- Abort loading the rest of the file if not in TouchMode
if (not gkinterface.IsTouchModeEnabled()) then return end

local initial_p = 0

local function list_release(event, id, percentW, percentH)
	local row_count = project_tbl.list_count
	local row_h = project_tbl.display[1][1].h
	
	if (initial_p > percentH) then
		-- Swipe Up
		if ((initial_p - percentH) < ((row_h / 4) / (row_h * row_count))) then
			-- Tap
			percentH = initial_p
		else
			gkinterface.GKProcessCommand("targetprev")
		end
	else
		-- Swipe Down
		if ((percentH - initial_p) < ((row_h / 4) / (row_h * row_count))) then
			-- Tap
			percentH = initial_p
		else
			gkinterface.GKProcessCommand("targetnext")
		end
	end
		
	if (percentH == initial_p) then
		for i = 1, row_count do
			if (percentH > ((row_h * (i - 1)) / (row_h * row_count)) and percentH < (row_h * i) / (row_h * row_count)) then
				--gkinterface.GKProcessCommand("target"..i)
				change_target(i)
				break
			end
		end
	end
	initial_p = 0
end

local function filter_release(event, id, percentW, percentH)
	local column_count = 4
	local column_w = project_tbl.filters[1].w
	if (initial_p > percentW) then
		-- Swipe Left
		if ((initial_p - percentW) < ((column_w / 4) / (column_w * column_count))) then
			-- Tap
			percentW = initial_p
		else
			gkinterface.GKProcessCommand("cyclefilter "..(project_tbl.filter - 1))
		end
	else
		-- Swipe Right
		if ((percentW - initial_p) < ((column_w / 4) / (column_w * column_count))) then
			-- Tap
			percentW = initial_p
		else
			gkinterface.GKProcessCommand("cyclefilter")
		end
	end
		
	if (percentW == initial_p) then
		for i = 1, column_count do
			if (percentW > ((column_w * (i - 1)) / (column_w * column_count)) and percentW < (column_w * i) / (column_w * column_count)) then
				if (project_tbl.filter == i - 1) then
					gkinterface.GKProcessCommand("cyclefactionfilter")
				else
					gkinterface.GKProcessCommand("cyclefilter "..(i - 1))
				end
				break
			end
		end
	end
	initial_p = 0
	
end

project_tbl.Events.TOUCH_PRESSED = function(_, event, id, percentW, percentH)
	if     (id == project_tbl.listregion) then
		initial_p = percentH
	elseif (id == project_tbl.filterregion) then
		initial_p = percentW
	end
end

project_tbl.Events.TOUCH_RELEASED = function(_, event, id, percentW, percentH)
	if     (id == project_tbl.listregion) then
		list_release(event, id, percentW, percentH)

	elseif (id == project_tbl.filterregion) then
		filter_release(event, id, percentW, percentH)

	end
end

RegisterEvent(project_tbl.Events, "TOUCH_PRESSED")
RegisterEvent(project_tbl.Events, "TOUCH_RELEASED")
