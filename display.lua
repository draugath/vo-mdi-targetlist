local project_tbl, scan_sector = ...

function project_tbl:BuildDisplay()
	local num_width, health_width, name_width, distance_width
	local fontsize = (self.fontsize or Font.Default) * HUD_SCALE

	local function ship_display(num, health, name, distance)
		num = num or ""
		health = health or ""
		name = name or ""
		name_width = name_width or self.name_width
		distance = distance or ""
		
		num = iup.label{title = num, size = num_width, font = fontsize, expand = "NO"}
		health = iup.label{title = health, size = health_width, font = fontsize, expand = "NO"} 
		name = iup.text{value = name, size = name_width, font = fontsize, expand = "NO", active = "NO", border = "NO", bgcolor = "0 0 0 255"}
		distance = iup.label{title = distance, size = distance_width, font = fontsize, alignment = "ARIGHT", expand = "NO"}
		
		local shield = iup.stationprogressbar{
			visible = "NO", active = "NO", expand = "HORIZONTAL", title = "",
			size = "x"..math.ceil(fontsize/4),
			minvalue = 0, maxvalue = 100, uppercolor = "88 88 88 255 *", lowercolor = "65 65 255 255 *",
			value = 0, uv = "0 0.25 1 0.75"
		}
		
		local highlight = iup.label{title = "", size = "", expand = "YES", bgcolor = "0 0 0 0 *"}
		
		local display = iup.zbox{
			highlight,
			iup.hbox{
				num, 
				health, 
				iup.zbox{
					iup.vbox{
						iup.fill{size = math.ceil(fontsize*3/5)},
						shield,
					},
					name, 
					all = "YES",
				}, 
				distance, 
				gap = 5, 
				--size = "200x",
			},
			alignment = "CENTER",
			all = "YES",
		}

		function display:GetWidth() return num_width + health_width + name_width + distance_width end
			
		if (not health_width) then
			num:map();      num_width = tonumber(num.w) -- .size:match("(%d+)x"))
			health:map();   health_width = tonumber(health.w) -- .size:match("(%d+)x"))
			name:map();     name_width = name_width or tonumber(name.w) -- .size:match("(%d+)x"))
			distance:map(); distance_width = tonumber(distance.w) -- .size:match("(%d+)x"))
			num.title = ""
			name.value = name.value:gsub("W", "")
			name.size = name_width
		end
		return display
	end

	local display_elements = {ship_display(99, "Health", "Name"..string.rep("W", 10), "Distance"),}
	for i = 1, self.list_count do
		table.insert(display_elements, ship_display(i))
	end
	self.display = iup.vbox(display_elements)

	local filter_height_adjust = fontsize > 13 and 4/5 or 1
	local filter_width = self.display[1]:GetWidth() / 4

	self.filters = iup.hbox{
		iup.label{title = "All", font = fontsize, alignment = "ACENTER", expand = "YES", size = filter_width.."x"..(fontsize*filter_height_adjust)},
		iup.label{title = "NPC", font = fontsize, alignment = "ACENTER", expand = "YES", size = filter_width.."x"..(fontsize*filter_height_adjust)},
		iup.label{title = "PC", font = fontsize, alignment = "ACENTER", expand = "YES", size = filter_width.."x"..(fontsize*filter_height_adjust)},
		iup.label{title = "Cap", font = fontsize, alignment = "ACENTER", expand = "YES", size = filter_width.."x"..(fontsize*filter_height_adjust)},
	}

	local filter_box = iup.zbox{
		self.filters,
		iup.label{title = "", font = fontsize, size = "x"..(fontsize*filter_height_adjust), expand = "YES", bgcolor = "255 0 0 "..(project_tbl.invert_faction_filter and 35 or 0)},
		margin = "0x0",
		all = "YES"
	}

	self.display_container = iup.hudrightframe{
		iup.vbox{
			self.display, 
			filter_box,
		},
		cx = (self.x or HUD.selfinfoframe.x) - self.display[1]:GetWidth() - 50, 
		cy = (self.y or HUD.groupinfoframe.y), 
		expand = "NO",
	}

	iup.Append(iup.GetNextChild(HUD.cboxlayer), project_tbl.display_container)
	HUD.cboxlayer:map()
	HUD.cboxlayer.REFRESH = "YES"

	
	function self.display_container.update_display()
		self.current_target_listindex = nil
		for i = 1, self.list_count do
			local health, shield, name = "", "", ""
			
			if (self.shiplist[i]) then
				local charid = self.shiplist[i].charid
				local nodeid = GetPlayerNodeID(charid)
				name = self.shiplist[i].name
				self.shiplist[i].nodeid = nodeid
				self.shiplist[i].objectid = GetPrimaryShipIDOfPlayer(charid)
				health, shield = GetPlayerHealth(charid)
				health = math.ceil(health or 0)
				local guild = GetGuildTag(charid)
				if (guild and guild ~= "") then name = "["..tostring(guild).."] "..name end
				
				local row = self.display[i + project_tbl.list_offset][2]
				row[2].title = health.."%"
				row[2].fgcolor = calc_health_color(health * 0.01)
				row[3][1][2].visible = shield and "YES" or "NO"
				row[3][2].value = name
				row[3][2].caret = 1
				row[3][2].fgcolor = FactionColor_RGB[self.shiplist[i].factionid]
				row[4].title = self.shiplist[i].distance
				self.display[i + project_tbl.list_offset].visible = "YES"
				
				if (shield) then
					shield = math.ceil(shield)
					row[3][1][2].value = shield
				end

				if (nodeid == self.current_target_nodeid) then
					self.display[i + project_tbl.list_offset][1].bgcolor = self.current_target_color
					self.current_target_listindex = i
				else
					self.display[i + project_tbl.list_offset][1].bgcolor = "0 0 0 0 *"
				end
					
			else
				self.display[i + project_tbl.list_offset].visible = "NO"
			end
		end
	end
	
	function self.filters:SetHighlight(index, onoff)
		if (onoff) then
			self[index].bgcolor = FactionColor_RGB[project_tbl.faction_filter]
			if (project_tbl.faction_filter == 0) then
				self[index].fgcolor = "40 40 40 255 *"
			else
				self[index].fgcolor = "255 255 255 255 *"
			end
		else
			self[index].bgcolor = "0 0 0 0 *"
			self[index].fgcolor = "255 255 255 255 *"
		end
	end

	self.filters:SetHighlight(self.filter + 1, true)
	
end

function project_tbl:DestroyDisplay()
	project_tbl.refresh_timer:Kill()
	iup.Detach(project_tbl.display_container)
	iup.Destroy(project_tbl.display_container)
	project_tbl.display_container = nil
	project_tbl.display = nil
	project_tbl.filters = nil
end

function project_tbl:RebuildDisplay()
	self:DestroyDisplay()
	self:DestroyRegions()
	self:BuildDisplay()
	self.display[1][2][3][1][2].visible = "NO" -- Re-hide the progressbar in the header.  For some reason it doesn't hide properly on it's own.
	self:CreateRegions()
	scan_sector()
end
